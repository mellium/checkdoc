// Copyright 2022 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"go/ast"
	"go/token"
	"strings"

	"golang.org/x/tools/go/analysis"
)

func allFlag(name string) (*flag.FlagSet, *bool) {
	var all bool
	flags := flag.NewFlagSet(name, flag.ExitOnError)
	flags.BoolVar(&all, "all", false, "Scan all packages including _test and main packages")
	return flags, &all
}

func excludePackage(file *ast.File, pass *analysis.Pass) bool {
	return file.Name.Name == "main" || strings.HasSuffix(pass.Fset.Position(file.Pos()).Filename, "_test.go") || isGenerated(file)
}

// identDoc returns a new static analyzer that checks for documentation on
// exported comments.
// It does not check the format of the documentation, just that a doc comment
// exists.
func identDoc() *analysis.Analyzer {
	const name = "identdoc"
	flags, all := allFlag(name)

	return &analysis.Analyzer{
		Name:  name,
		Flags: *flags,
		Doc:   "report packages with documentation on exported identifiers",
		Run: func(pass *analysis.Pass) (interface{}, error) {
			for _, file := range pass.Files {
				if !*all && excludePackage(file, pass) {
					break
				}

				for _, decl := range file.Decls {
					var undoced bool
					switch node := decl.(type) {
					case *ast.FuncDecl:
						if ast.IsExported(receiverType(node)) && ast.IsExported(node.Name.Name) && node.Doc == nil {
							pass.Reportf(node.Name.NamePos, "no docs for function %v", node.Name.Name)
						}
					case *ast.GenDecl:
						listDoc := node.Doc
						var exported string
						for _, spec := range node.Specs {
							switch s := spec.(type) {
							case *ast.ImportSpec:
								// We don't require comments on imports.
							case *ast.ValueSpec:
								// Const and var declarations (or lists):
								for _, ident := range s.Names {
									if ast.IsExported(ident.Name) {
										exported = ident.Name
										break
									}
								}

								// If any name was exported, check that we either document the
								// item or the overall list.
								if exported != "" && s.Doc == nil && listDoc == nil {
									undoced = true
									pass.Reportf(node.Pos(), "no docs for var or const decl %q", exported)
								}
							case *ast.TypeSpec:
								if ast.IsExported(s.Name.Name) {
									exported = s.Name.Name
								}
								// For types we must document the overall list and the
								// individual types.
								if exported != "" && ((!node.Lparen.IsValid() && listDoc == nil) || (node.Lparen.IsValid() && s.Doc == nil)) {
									pass.Reportf(s.Pos(), "no docs for type %q", s.Name.Name)
								}
							}
						}
						// If any item in the list was exported, we expect the list to have
						// a comment (even if all the items in it are documented).
						if undoced && node.Lparen.IsValid() && exported != "" && listDoc == nil {
							pass.Reportf(node.TokPos, "no doc comment for list containing %q", exported)
						}
					}
				}
			}

			return nil, nil
		},
	}
}

// pkgDoc returns a new static analyzer that checks for package level
// documentation.
func pkgDoc() *analysis.Analyzer {
	const name = "pkgdoc"
	flags, all := allFlag(name)

	return &analysis.Analyzer{
		Name:  name,
		Flags: *flags,
		Doc:   "report packages with missing package level documentation",
		Run: func(pass *analysis.Pass) (interface{}, error) {
			var found bool
			var pos token.Pos
			for _, file := range pass.Files {
				if !*all && excludePackage(file, pass) {
					found = true
					break
				}

				// Pos is the location of any package decl, doesn't really matter which.
				pos = file.Package
				if file.Doc != nil {
					found = true
					break
				}
			}

			if !found {
				pass.Reportf(pos, "no docs for package %v", pass.Pkg.Path())
			}
			return nil, nil
		},
	}
}

// canonicalImport returns a new static analyzer that checks for a static import
// comment.
func canonicalImport() *analysis.Analyzer {
	const name = "canonicalImport"
	flags, all := allFlag(name)

	return &analysis.Analyzer{
		Name:  name,
		Flags: *flags,
		Doc:   "report packages with missing or malformed import comments",
		Run: func(pass *analysis.Pass) (interface{}, error) {
			var found bool
			var pos token.Pos

			for _, file := range pass.Files {
				if !*all && excludePackage(file, pass) {
					found = true
					break
				}

				// Pos is the location of any package decl, doesn't really matter which.
				pos = file.Package
				for _, c := range file.Comments {
					if len(c.List) == 1 && c.List[0].Text == fmt.Sprintf(`// import "%s"`, pass.Pkg.Path()) {
						position := pass.Fset.Position(pos)
						commentPosition := pass.Fset.Position(c.List[0].Slash)
						if position.Line == commentPosition.Line {
							found = true
							break
						}
					}
				}
			}

			if !found {
				pass.Reportf(pos, "no canonical import comment for package %v", pass.Pkg)
			}
			return nil, nil
		},
	}
}

func receiverType(fn *ast.FuncDecl) string {
	if fn.Recv == nil {
		return ""
	}
	switch e := fn.Recv.List[0].Type.(type) {
	case *ast.Ident:
		return e.Name
	case *ast.StarExpr:
		if id, ok := e.X.(*ast.Ident); ok {
			return id.Name
		}
	}
	return "invalid-type"
}

const (
	genHdr = "// Code generated "
	genFtr = " DO NOT EDIT."
)

// isGenerated reports whether the source file is generated code
// according the rules from https://golang.org/s/generatedcode.
func isGenerated(file *ast.File) bool {
	if len(file.Comments) == 0 {
		return false
	}
	comment := file.Comments[0].List[0].Text
	return strings.HasPrefix(comment, genHdr) && strings.HasSuffix(comment, genFtr) && len(comment) >= len(genHdr)+len(genFtr)
}
