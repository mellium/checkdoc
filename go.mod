module mellium.im/checkdoc

go 1.21

require (
	github.com/securego/gosec v0.0.0-20200401082031-e946c8c39989
	golang.org/x/tools v0.20.0
	honnef.co/go/tools v0.4.7
)

require (
	github.com/BurntSushi/toml v1.3.2 // indirect
	github.com/nbutton23/zxcvbn-go v0.0.0-20210217022336-fa2cb2858354 // indirect
	golang.org/x/exp/typeparams v0.0.0-20240416160154-fe59bbe5cc7f // indirect
	golang.org/x/mod v0.17.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/xerrors v0.0.0-20220907171357-04be3eba64a2 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
