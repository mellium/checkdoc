# checkdoc

[![Issue Tracker][badge]](https://mellium.im/issue)
[![Docs](https://pkg.go.dev/badge/mellium.im/legacy)](https://pkg.go.dev/mellium.im/legacy)
[![Chat](https://img.shields.io/badge/XMPP-users@mellium.chat-orange.svg)](https://mellium.chat)
[![License](https://img.shields.io/badge/license-FreeBSD-blue.svg)](https://opensource.org/licenses/BSD-2-Clause)

The `checkdoc` command scans Go packages for comment and documentation related
lints.
Currently the tool checks for:

- Packages that are missing documentation comments
- Exported identifiers that are missing documentation comments
- Malformed or missing canonical import comments
- Missing file header comments

If you'd like to contribute to the project, see [CONTRIBUTING.md].

To use `checkdoc` first install it:

```
go install mellium.im/checkdoc@latest
```

Then run `checkdoc -help` to find out more about using it.
For example, to run the file header check lint across an entire module:

```
checkdoc -fileheader.pattern='-' ./... <<EOF
Copyright \d\d\d\d The Mellium Contributors\.
Use of this source code is governed by the BSD 2-clause
license that can be found in the LICENSE file\.
EOF
```


## License

The package may be used under the terms of the BSD 2-Clause License a copy of
which may be found in the file "[LICENSE]".

[badge]: https://img.shields.io/badge/style-mellium%2fxmpp-green.svg?longCache=true&style=popout-square&label=issues
[CONTRIBUTING.md]: https://mellium.im/docs/CONTRIBUTING
[LICENSE]: https://codeberg.org/mellium/xmpp/src/branch/main/LICENSE
