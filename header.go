// Copyright 2022 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"

	"golang.org/x/tools/go/analysis"
)

// fileHeader returns a new static analyzer that checks all Go files for a
// copyright header that matches a pattern.
func fileHeader() *analysis.Analyzer {
	const name = "fileheader"
	flags := flag.NewFlagSet(name, flag.ExitOnError)
	var (
		headerFile string
		includeGen bool
	)
	flags.StringVar(&headerFile, "pattern", "", "a file containing a regular expression to match against header comments or - to use stdin")
	flags.BoolVar(&includeGen, "generated", includeGen, "do not skip files containing a code generated header")

	return &analysis.Analyzer{
		Name:  name,
		Flags: *flags,
		Doc:   "report files that are missing a header",
		Run: func(pass *analysis.Pass) (interface{}, error) {
			var input []byte
			var err error
			var exp *regexp.Regexp
			switch headerFile {
			case "":
				// Do not match regular expression
				goto afterparse
			case "-":
				// Use stdin
				input, err = io.ReadAll(os.Stdin)
				if err != nil {
					return nil, fmt.Errorf("error reading regex from stdin: %v", err)
				}
			default:
				// Load from file
				/* #nosec */
				input, err = os.ReadFile(headerFile)
				if err != nil {
					return nil, fmt.Errorf("error reading regex from stdin: %v", err)
				}
			}
			exp, err = regexp.Compile(string(input))
			if err != nil {
				return nil, err
			}
		afterparse:
			genExp := regexp.MustCompile("^Code generated .* DO NOT EDIT\\.\n?$")

			for _, file := range pass.Files {
				switch {
				case !strings.HasSuffix(pass.Fset.File(file.Package).Name(), ".go"):
					// Skip build files from the cache.
					// TODO: is there a better way to detect this than checking for the
					// .go extension, or is this a bug that could be fixed upstream?
					continue
				case len(file.Comments) == 0:
					pass.Reportf(file.Pos(), "no header comment found at start of file")
					continue
				case pass.Fset.Position(file.Comments[0].List[0].Slash).Offset != 0:
					pass.Reportf(file.Comments[0].List[0].Slash, "no header comment found at start of file")
					continue
				case !includeGen && genExp.MatchString(file.Comments[0].Text()):
					// Skip generated files.
					// TODO: this can technically appear anywhere in the file.
					continue
				}

				if exp != nil && !exp.MatchString(file.Comments[0].Text()) {
					pass.Reportf(file.Comments[0].List[0].Slash, "header comment does not match")
				}
			}

			return nil, nil
		},
	}
}
