// Copyright 2022 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

// The checkdoc command analyzes Go source code to ensure package level docs are
// present.
package main // import "git.sr.ht/~samwhited/checkdoc"

import (
	"golang.org/x/tools/go/analysis/multichecker"
)

func main() {
	multichecker.Main(
		canonicalImport(),
		fileHeader(),
		pkgDoc(),
		identDoc(),
	)
}
