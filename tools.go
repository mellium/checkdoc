// Copyright 2022 The Mellium Contributors.
// Use of this source code is governed by the BSD 2-clause
// license that can be found in the LICENSE file.

//go:build tools
// +build tools

package main

import (
	_ "github.com/securego/gosec/cmd/gosec"
	_ "honnef.co/go/tools/cmd/staticcheck"
)
